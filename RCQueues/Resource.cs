﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RCQueues
{
    public class Resource
    {
        public string Id { get; set; }
        public string Desc { get; set; }
        public double Capacity { get; set; }
        public override string ToString()
        {
            return string.Format("{0} ({1})", Id, Desc);
        }
    }
}
