﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RCQueues
{    
    public class Requirement
    {
        public Func<Resource, bool> Condition { get; set; }
        public double Quantity { get; set; }
    }
}
