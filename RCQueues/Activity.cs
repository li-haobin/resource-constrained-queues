﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RCQueues
{
    public class Activity
    {
        public string Id { get; set; }
        public string Desc { get; set; }
        public List<Requirement> Requirements { get; set; }
        public Func<Random, TimeSpan> Duration { get; set; }
        public Func<Random, object, Activity> Succeeding { get; set; }
    }
}
