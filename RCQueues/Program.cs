﻿using O2DESNet;
using O2DESNet.Distributions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RCQueues
{
    class Program
    {
        static void Main()
        {
            var activities = new Activity[]
            {
                // flow 1
                new Activity
                {
                    Id = "0",
                    Requirements = new List<Requirement>
                    {
                        new Requirement{ Condition = res => Convert.ToInt32(res.Id) > 5, Quantity = 2 },
                        new Requirement{ Condition = res => Convert.ToInt32(res.Id) < 3, Quantity = 1 },
                    },
                    Duration = rs => Exponential.Sample(rs, TimeSpan.FromMinutes(5)),
                },
                new Activity
                {
                    Id = "1",
                    Requirements = new List<Requirement>
                    {
                        new Requirement{ Condition = res => Convert.ToInt32(res.Id) > 7, Quantity = 1 },
                        new Requirement{ Condition = res => Convert.ToInt32(res.Id) > 2 && Convert.ToInt32(res.Id) < 5, Quantity = 1 },
                    },
                    Duration = rs => Exponential.Sample(rs, TimeSpan.FromMinutes(5)),
                },
                new Activity
                {
                    Id = "2",
                    Requirements = new List<Requirement>
                    {
                        new Requirement{ Condition = res => Convert.ToInt32(res.Id) > 7 && Convert.ToInt32(res.Id) < 10, Quantity = 1 },
                        new Requirement{ Condition = res => Convert.ToInt32(res.Id) > 2 && Convert.ToInt32(res.Id) < 5, Quantity = 1 },
                    },
                    Duration = rs => Exponential.Sample(rs, TimeSpan.FromMinutes(5)),
                },
                /// flow 2
                new Activity
                {
                    Id = "3",
                    Requirements = new List<Requirement>
                    {
                        new Requirement{ Condition = res => Convert.ToInt32(res.Id) > 6 && Convert.ToInt32(res.Id) < 8, Quantity = 0.3 },
                        new Requirement{ Condition = res => Convert.ToInt32(res.Id) > -1 && Convert.ToInt32(res.Id) < 4, Quantity = 1 },
                    },
                    Duration = rs => Exponential.Sample(rs, TimeSpan.FromMinutes(5)),
                },
                new Activity
                {
                    Id = "4",
                    Requirements = new List<Requirement>
                    {
                        new Requirement{ Condition = res => Convert.ToInt32(res.Id) > 3 && Convert.ToInt32(res.Id) < 11, Quantity = 0.2 },
                        new Requirement{ Condition = res => Convert.ToInt32(res.Id) > 1 && Convert.ToInt32(res.Id) < 6, Quantity = 1 },
                    },
                    Duration = rs => Exponential.Sample(rs, TimeSpan.FromMinutes(5)),
                },
                new Activity
                {
                    Id = "5",
                    Requirements = new List<Requirement>
                    {
                        new Requirement{ Condition = res => Convert.ToInt32(res.Id) > -1 && Convert.ToInt32(res.Id) < 3, Quantity = 0.2 },
                        new Requirement{ Condition = res => Convert.ToInt32(res.Id) > 6 && Convert.ToInt32(res.Id) < 11, Quantity = 1 },
                    },
                    Duration = rs => Exponential.Sample(rs, TimeSpan.FromMinutes(5)),
                },
            };
            activities[0].Succeeding = (rs, o) => activities[1];
            activities[1].Succeeding = (rs, o) => activities[2];
            activities[2].Succeeding = (rs, o) => null;
            activities[3].Succeeding = (rs, o) => activities[4];
            activities[4].Succeeding = (rs, o) => activities[5];
            activities[5].Succeeding = (rs, o) => null;
            int count = 0;
            var config = new Model<int>.Statics
            {
                Activities = activities,
                Starters = new Activity[] { activities[0], activities[3] },
                Resources = Enumerable.Range(0, 10).Select(id => new Resource { Id = id.ToString(), Capacity = 1 }).ToArray(),
                InterArrivalTime = rs => Exponential.Sample(rs, TimeSpan.FromMinutes(10)),
                CreateLoad = rs => count++,
            };
            var state = new Model<int>(config, 0);
            var sim = new Simulator(state);
            while (true)
            {
                sim.Run(1);
                Console.Clear();
                Console.WriteLine(sim.ClockTime);
                sim.WriteToConsole();
                Console.ReadKey();
            }
        }
    }
}
