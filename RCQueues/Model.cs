﻿using O2DESNet;
using O2DESNet.Distributions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RCQueues
{
    public class Model<TLoad> : State<Model<TLoad>.Statics>
    {
        #region Statics
        public class Statics : Scenario
        {
            /******************************************************/
            /* All static properties shall be public,             */
            /* for both getter and setter.                        */
            /******************************************************/
            public Activity[] Starters { get; set; }
            public Activity[] Activities { get; set; }
            public Resource[] Resources { get; set; }
            public Func<Random, TimeSpan> InterArrivalTime { get; set; }
            public Func<Random, TLoad> CreateLoad { get; set; }
        }
        #endregion

        #region Dynamics
        /**********************************************************/
        /* All dynamic properties shall have only public getter,  */
        /* where setter should remain as private.                 */
        /**********************************************************/
        /// <summary>
        /// Map the activity to the list of quantified resource 
        /// This is to be formed and fixed at init function.
        /// </summary>
        public Dictionary<Activity, List<Resource>> ResourcePool { get; private set; }
        /// <summary>
        /// Map occupied resource to the load, activity, requirement and quantity that is occupying it
        /// </summary>
        public Dictionary<Resource, List<Tuple<TLoad, Activity, Requirement, double>>> Occupation { get; private set; }
        /// <summary>
        /// Map load and requirements to the resources and quantity it is occupying
        /// </summary>
        public Dictionary<TLoad, Dictionary<Requirement, List<Tuple<Resource, double>>>> Occupying { get; private set; }
            = new Dictionary<TLoad, Dictionary<Requirement, List<Tuple<Resource, double>>>>();
        /// <summary>
        /// Map occupied resource to the list of loads pending to request it, and the next activity of the load
        /// </summary>
        public Dictionary<Resource, HashSet<TLoad>> PendingLoads { get; private set; }
            = new Dictionary<Resource, HashSet<TLoad>>();
        /// <summary>
        /// Track the current activity of each load flow in the model
        /// </summary>
        public Dictionary<TLoad, Activity> Currents { get; private set; } = new Dictionary<TLoad, Activity>();
        /// <summary>
        /// Record the activity to move to for each load 
        /// </summary>
        public Dictionary<TLoad, Activity> MoveTos { get; private set; } = new Dictionary<TLoad, Activity>();
        /// <summary>
        /// Enquiry for remaining capacity of given resource, for given load and activity
        /// </summary>
        private double RemainingCapcity(Resource resource, TLoad load, Activity activity)
        {
            return resource.Capacity - Occupation[resource]
                .Where(tpl => !tpl.Item1.Equals(load)) /// quantity occupied by other loads
                .Sum(tpl => tpl.Item4); /// sum up the quantity of occupation
        }
        /// <summary>
        /// Request resources for a given activity of a load
        /// </summary>
        /// <returns>Map the requirement to the list of resources and corresponding quantity to occupy</returns>
        private Dictionary<Requirement, List<Tuple<Resource, double>>> RqstResources(Activity activity, TLoad load)
        {
            var resources = new Dictionary<Requirement, List<Tuple<Resource, double>>>();
            var staged = ResourcePool[activity].ToDictionary(res => res, res => 0d);
            foreach (var req in activity.Requirements)
            {
                var pool = ResourcePool[activity].Where(res => req.Condition(res))
                    .Select(res => new Tuple<Resource, double>(res, RemainingCapcity(res, load, activity) - staged[res]))
                    .OrderByDescending(i => i.Item2).ToList(); /// map all available resource to its remaining capacity
                if (pool.Sum(i => i.Item2) < req.Quantity) return null; /// no sufficient resource
                var toRqst = new List<Tuple<Resource, double>>();
                while (toRqst.Sum(i => i.Item2) < req.Quantity)
                {
                    var qtt = Math.Min(pool.First().Item2, req.Quantity - toRqst.Sum(i => i.Item2));
                    toRqst.Add(new Tuple<Resource, double>(pool.First().Item1, qtt));
                    staged[pool.First().Item1] += qtt;
                    pool.RemoveAt(0);
                }
                resources.Add(req, toRqst);
            }
            return resources;
        }
        public int TotalNLoads { get; private set; } = 0;
        public HashSet<TLoad> AllLoads { get; private set; } = new HashSet<TLoad>();
        /// <summary>
        /// The stage indices which is strictly increasing for each successful move, 
        /// for all loads in the system.
        /// </summary>
        public Dictionary<TLoad, int> StageIndices { get; private set; } = new Dictionary<TLoad, int>();
        #endregion

        #region Events
        private abstract class InternalEvent : Event<Model<TLoad>, Statics> { } /// event adapter 

        /**********************************************************/
        /* All internal events shall be private,                  */
        /* and inherite from InternalEvent as defined above       */
        /**********************************************************/
        private class ArriveEvent : InternalEvent
        {
            public override void Invoke()
            {
                var load = Config.CreateLoad(DefaultRS);
                This.AllLoads.Add(load);
                This.Currents[load] = null;
                This.MoveTos[load] = Uniform.Sample(DefaultRS, Config.Starters);
                This.StageIndices.Add(load, 0);
                Execute(new AtmptMoveEvent
                {
                    Load = load,
                    StageIndex = This.StageIndices[load],
                });
                Schedule(new ArriveEvent(), Config.InterArrivalTime(DefaultRS));
            }
        }
        private class AtmptMoveEvent : InternalEvent
        {
            internal TLoad Load { get; set; }
            internal int StageIndex { get; set; }
            public override void Invoke()
            {
                if (!This.AllLoads.Contains(Load) || StageIndex < This.StageIndices[Load]) return; /// prevent duplicated events
                var released = new Dictionary<Resource, double>();
                if (This.MoveTos[Load] == null) This.MoveTos[Load] = This.Currents[Load].Succeeding(DefaultRS, Load);
                if (This.MoveTos[Load] == null)
                {
                    /// move out from the last activity
                    released = Release();
                    This.AllLoads.Remove(Load);
                    This.StageIndices.Remove(Load);
                }
                else
                {
                    var rqst = This.RqstResources(This.MoveTos[Load], Load);
                    if (rqst != null)
                    {
                        /// Has sufficient requested resources                        
                        if (StageIndex > 0) released = Release();
                        /// creat resource occupation                        
                        This.Occupying.Add(Load, rqst);
                        foreach (var i in rqst)
                            foreach (var j in i.Value)
                            {
                                var res = j.Item1;
                                var qtt = j.Item2;
                                This.Occupation[res].Add(
                                    new Tuple<TLoad, Activity, Requirement, double>(Load, This.MoveTos[Load], i.Key, qtt));
                                if (released.ContainsKey(res))
                                {
                                    released[res] -= qtt;
                                    if (released[res] <= 0) released.Remove(res);
                                }
                            }
                        /// clear from pending list
                        foreach (var res in This.ResourcePool[This.MoveTos[Load]]
                            .Where(res => This.PendingLoads.ContainsKey(res)))
                            This.PendingLoads[res].Remove(Load);
                        /// Schedule for next move
                        Schedule(new AtmptMoveEvent
                        {
                            Load = Load,
                            StageIndex = ++This.StageIndices[Load],
                        },
                        This.MoveTos[Load].Duration(DefaultRS));
                        /// update activity record for the load
                        This.Currents[Load] = This.MoveTos[Load];
                        This.MoveTos[Load] = null;
                    }
                    else
                    {
                        /// Has insufficient requested resources
                        foreach (var res in This.ResourcePool[This.MoveTos[Load]])
                            This.PendingLoads[res].Add(Load);
                    }
                }
                /// Attempt to move for loads pending for released resources
                foreach (var load in released.SelectMany(i => This.PendingLoads[i.Key]).Distinct())
                {
                    var t = This.Currents[load];
                    if (This.AllLoads.Contains(load))
                        Schedule(new AtmptMoveEvent
                        {
                            Load = load,
                            StageIndex = This.StageIndices[load],
                        });
                }
            }
            private Dictionary<Resource, double> Release()
            {
                var released = new Dictionary<Resource, double>();
                foreach (var occup in This.Occupying[Load])
                {
                    foreach (var tup in occup.Value)
                    {
                        if (!released.ContainsKey(tup.Item1)) released.Add(tup.Item1, 0);
                        released[tup.Item1] += tup.Item2;
                    }
                }
                This.Occupying.Remove(Load);
                foreach (var res in released.Keys)
                {
                    var list = This.Occupation[res];
                    int i = 0;
                    while (i < list.Count)
                    {
                        if (list[i].Item1.Equals(Load)) list.RemoveAt(i);
                        else i++;
                    }
                }
                return released;
            }
        }
        #endregion
        
        public Model(Statics config, int seed, string tag = null) : base(config, seed, tag)
        {
            Name = "Model";

            Init();
            InitEvents.Add(new ArriveEvent { This = this });
        }
        private void Init()
        {
            ResourcePool = Config.Activities.ToDictionary(a => a, 
                a => a.Requirements.SelectMany(req => Config.Resources.Where(res => req.Condition(res))).Distinct().ToList());
            PendingLoads = Config.Resources.ToDictionary(res => res, res => new HashSet<TLoad>());
            Occupation = Config.Resources.ToDictionary(res => res, res => new List<Tuple<TLoad, Activity, Requirement, double>>());
        }

        public override void WarmedUp(DateTime clockTime)
        {
            base.WarmedUp(clockTime);
        }

        public override void WriteToConsole(DateTime? clockTime = null)
        {
            foreach (var load in AllLoads)
            {
                Console.Write("Id: {0}\tAct_Id: {1}\t", load, Currents[load] == null ? "null" : Currents[load].Id);
                if (Occupying.ContainsKey(load))
                    foreach (var tup in Occupying[load].SelectMany(i => i.Value))
                        Console.Write("Res#{0}({1}) ", tup.Item1.Id, tup.Item2);
                Console.WriteLine();
            }
            Console.WriteLine();
            foreach (var res in Config.Resources)
            {
                Console.Write("Id: {0}\tOccupied: ", res.Id);
                foreach (var tup in Occupation[res]) Console.Write("{0} ", tup.Item1.ToString());
                Console.Write("\tPending:");
                foreach (var load in PendingLoads[res]) Console.Write("{0} ", load);
                Console.WriteLine();
            }
        }
    }
}
